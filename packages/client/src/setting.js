export default {
  api: {
    url: "//localhost:5050/api/v0",
    todos: "/todo",
    todoRemove: "/todo/remove",
    itemDone: "/todo/mark-item",
    itemAdd: "/todo/add-item",
    itemRemove: "/todo/remove-item",
    itemSortByName: "/todo/sort-item-name",
  },
}