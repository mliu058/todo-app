import React, { useState, useEffect } from "react"
import axios from "axios"
import { Form, Button, Container, Row, Col, ListGroup } from "react-bootstrap"
import { BsFillTrashFill } from "react-icons/bs"

import setting from "./setting"

function App() {
  const { api } = setting

  const [todos, setTodos] = useState(null)
  const [currTodo, setCurrTodo] = useState(null)

  const [showAddList, setShowAddList] = useState(false)
  const [listName, setListName] = useState("")

  const [showAddItem, setShowAddItem] = useState(false)
  const [newItem, setNewItem] = useState("")

  const [isLoading, setLoading] = useState(false)

  const initErrors = {
    createList: false,
    addItem: false,
  }
  const [errors, setError] = useState(initErrors)

  useEffect(() => {
    fetchTodos()
  }, [])

  const reset = () => {
    setError(initErrors)
    setListName("")
    setNewItem("")
  }

  const sortItems = (todoId, items) => {
    const sorted = currTodo.items.sort((a, b) => (a.text > b.text ? 1 : -1))

    setCurrTodo({ ...currTodo, items: sorted })

    sortItemName(todoId, items)
  }

  //* api calls
  const sortItemName = async (todoId, items) => {
    try {
      setLoading(true)

      const res = await axios.post(`${api.url}${api.itemSortByName}`, {
        todoId,
        items,
      })

      if (!res) return console.log("sortItemName err")

      reset()
      fetchTodos()
      setLoading(false)
    } catch (err) {
      console.log("sortItemName:", err)
      setLoading(false)
    }
  }

  const fetchTodos = async () => {
    try {
      const res = await axios.get(`${api.url}${api.todos}`)

      if (!res) return console.log("fetchTodos err")

      setTodos(res.data.todos)
      setCurrTodo(res.data.todos[0])
    } catch (err) {
      console.log("fetchTodos:", err)
    }
  }

  const updateItemDone = async (todoId, itemId) => {
    try {
      const res = await axios.post(`${api.url}${api.itemDone}`, {
        todoId,
        itemId,
      })

      if (!res) return console.log("updateItemDone err")

      fetchTodos()
    } catch (err) {
      console.log("updateItemDone:", err)
    }
  }

  const createList = async () => {
    if (!listName) return setError({ ...errors, createList: true })

    try {
      setLoading(true)

      const res = await axios.post(`${api.url}${api.todos}`, {
        name: listName,
      })

      if (!res) return console.log("createList err")

      reset()
      fetchTodos()
      setLoading(false)
    } catch (err) {
      console.log("createList:", err)
      setLoading(false)
    }
  }

  const removeList = async todoId => {
    try {
      const res = await axios.post(`${api.url}${api.todoRemove}`, {
        todoId,
      })

      if (!res) return console.log("removeList err")

      reset()
      fetchTodos()
    } catch (err) {
      console.log("removeList:", err)
    }
  }

  const createItem = async () => {
    if (!newItem) return setError({ ...errors, addItem: true })

    try {
      setLoading(true)

      const res = await axios.post(`${api.url}${api.itemAdd}`, {
        todoId: currTodo.id,
        item: newItem,
      })

      if (!res) return console.log("createItem err")

      reset()
      fetchTodos()
      setLoading(false)
      setShowAddItem(false)
    } catch (err) {
      console.log("createItem:", err)
      setLoading(false)
    }
  }

  const removeItem = async itemId => {
    try {
      setLoading(true)

      const res = await axios.post(`${api.url}${api.itemRemove}`, {
        todoId: currTodo.id,
        itemId,
      })

      if (!res) return console.log("createItem err")

      reset()
      fetchTodos()
      setLoading(false)
      setShowAddItem(false)
    } catch (err) {
      console.log("createItem:", err)
      setLoading(false)
    }
  }

  const TodoList = () => {
    if (todos === null) {
      return <div>Loading...</div>
    } else if (todos.length === 0) {
      return <div>Create a todo</div>
    } else if (todos.length > 0) {
      return todos.map((itm, idx) => (
        <ListGroup.Item
          className={`todo__link ${
            currTodo && itm.id === currTodo.id ? "active" : ""
          }`}
          key={idx}
        >
          <p
            onClick={() => {
              setCurrTodo(itm)
              setShowAddList(false)
              setShowAddItem(false)
              reset()
            }}
            className="todo__linkName mb-0"
          >
            {itm.name}
          </p>
          <BsFillTrashFill
            className="todo__itemDelete"
            onClick={() => removeList(itm.id)}
          />
        </ListGroup.Item>
      ))
    }
  }

  const CreateTodoItems = () => {
    return (
      <div className="todo__item">
        <Button
          onClick={() => {
            reset()
            setShowAddItem(true)
          }}
          variant="link"
          className="p-0"
        >
          Create a an item
        </Button>
      </div>
    )
  }

  const TodoItems = ({ todo }) => {
    if (todo === null) {
      return <div>Loading...</div>
    } else if (todo.items.length === 0) {
      return null
    } else if (todo.items.length > 0) {
      return todo.items.map((itm, idx) => (
        <div className={`todo__item ${itm.isDone ? "done" : ""}`} key={idx}>
          <Form.Check
            className="todo__itemCheckbox"
            custom
            defaultChecked={itm.isDone}
            onClick={() => updateItemDone(todo.id, itm.id)}
            type={"checkbox"}
            id={`custom-${itm.id}`}
          />
          <p className="todo__itemText mb-0">{itm.text}</p>
          <BsFillTrashFill
            className="todo__itemDelete"
            onClick={() => removeItem(itm.id)}
          />
        </div>
      ))
    }
  }

  return (
    <>
      <Container fluid className="todo">
        <Row className="todo__row">
          <Col xs={3}>
            <Button
              onClick={() => {
                reset()
                setShowAddList(true)
              }}
              variant="info"
              className="todo__add"
            >
              Add List
            </Button>
            <p className="small m-1">My Lists</p>
            <ListGroup className="todo__list">
              <TodoList />
            </ListGroup>
          </Col>
          <Col xs={9}>
            <div className="todo__content">
              {showAddList ? (
                <div className="todo__new">
                  <Form.Group className="mb-5">
                    <Form.Label>List Name</Form.Label>
                    <Form.Control
                      required
                      isInvalid={errors?.createList}
                      onChange={e => {
                        setListName(e.target.value)
                      }}
                      value={listName}
                      type="text"
                      placeholder="List Name"
                    />
                    {errors?.createList && (
                      <Form.Control.Feedback type="invalid">
                        Please input a list name
                      </Form.Control.Feedback>
                    )}
                  </Form.Group>

                  <Button onClick={createList} className="w-100">
                    {isLoading ? "Loading..." : "Create"}
                  </Button>
                </div>
              ) : showAddItem ? (
                <div className="todo__new">
                  <Form.Group className="mb-5">
                    <Form.Label>Item</Form.Label>
                    <Form.Control
                      required
                      isInvalid={errors?.addItem}
                      onChange={e => {
                        setNewItem(e.target.value)
                      }}
                      value={newItem}
                      type="text"
                      placeholder="Item"
                    />

                    {errors?.addItem && (
                      <Form.Control.Feedback type="invalid">
                        Please type your item
                      </Form.Control.Feedback>
                    )}
                  </Form.Group>

                  <Button onClick={createItem} className="w-100">
                    {isLoading ? "Loading..." : "Create"}
                  </Button>
                </div>
              ) : (
                <>
                  <Button variant="link" onClick={() => sortItems(currTodo.id, currTodo.items)} className="">
                    Sort name
                  </Button>
                  <TodoItems todo={currTodo} />
                  <CreateTodoItems />
                </>
              )}
            </div>
          </Col>
        </Row>
      </Container>
    </>
  )
}

export default App
