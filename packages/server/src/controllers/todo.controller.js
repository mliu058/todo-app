const fs = require("fs")
const path = require("path")
const fileName = path.resolve(__dirname, "../data/store.json")
const file = require(fileName)
const { v4: uuidv4 } = require("uuid")
// const moment = require("moment")

//todo: get todos
const getTodos = (req, res) => {
  try {
    if (!file)
      return res
        .status(400)
        .send({ status: false, message: `data not found: \n ${err}` })

    fs.readFile(fileName, "utf8", (err, data) => {
      if (err) {
        return res
          .status(400)
          .send({ status: false, message: `writing data error: \n ${err}` })
      } else {
        return res.status(200).send(data)
      }
    })
  } catch (err) {
    res.status(400).send({ status: false, message: "cannot get todos" })
  }
}

//todo: create todo
const createTodo = (req, res) => {
  try {
    let store = file

    const { name } = req.body

    if (!name)
      return res
        .status(400)
        .send({ status: false, message: "todo name not found" })

    //
    const newTodo = {
      id: uuidv4(),
      name,
      items: [],
    }

    if (!store.todos) {
      //* create array to store todo list
      store = { ...store, todos: [...todos, newTodo] }
    } else {
      //* update todo array
      store.todos.push(newTodo)
    }

    fs.writeFile(fileName, JSON.stringify(store), err => {
      if (err) {
        return res
          .status(400)
          .send({ status: false, message: `writing data error: \n ${err}` })
      } else {
        return res
          .status(200)
          .send({ status: true, message: `updated successfully` })
      }
    })
  } catch (err) {
    res.status(400).send({ status: false, message: "cannot add item" })
  }
}

//todo: remove todo
const removeTodo = (req, res) => {
  try {
    let store = file

    const { todoId } = req.body

    if (!todoId)
      return res.status(400).send({ status: false, message: "id not found" })

    //* remove item in todo
    store.todos = store.todos.filter(todo => todo.id !== todoId)

    fs.writeFile(fileName, JSON.stringify(store), err => {
      if (err) {
        return res
          .status(400)
          .send({ status: false, message: `writing data error: \n ${err}` })
      } else {
        return res
          .status(200)
          .send({ status: true, message: `updated successfully` })
      }
    })
  } catch (err) {
    res.status(400).send({ status: false, message: "cannot remove todo" })
  }
}

//todo: add item
const addItem = (req, res) => {
  try {
    let store = file

    const { todoId, item } = req.body

    if (!todoId)
      return res
        .status(400)
        .send({ status: false, message: "todo id not found" })

    //* new item
    const newItem = {
      id: uuidv4(),
      isDone: false,
      text: item,
      dueDate: Date.now(),
    }

    //* add new item in todo
    store.todos.map(todo => {
      if (todo.id === todoId) {
        todo.items = [...todo.items, newItem]
      }
    })

    fs.writeFile(fileName, JSON.stringify(store), err => {
      if (err) {
        return res
          .status(400)
          .send({ status: false, message: `writing data error: \n ${err}` })
      } else {
        return res
          .status(200)
          .send({ status: true, message: `updated successfully` })
      }
    })
  } catch (err) {
    res.status(400).send({ status: false, message: "cannot add item" })
  }
}

//todo: remove item
const removeItem = (req, res) => {
  try {
    let store = file

    const { todoId, itemId } = req.body

    if (!todoId || !itemId)
      return res.status(400).send({ status: false, message: "id not found" })

    //* remove item in todo
    store.todos.map(todo => {
      if (todo.id === todoId) {
        todo.items = todo.items.filter(item => item.id !== itemId)
      }
    })

    fs.writeFile(fileName, JSON.stringify(store), err => {
      if (err) {
        return res
          .status(400)
          .send({ status: false, message: `writing data error: \n ${err}` })
      } else {
        return res
          .status(200)
          .send({ status: true, message: `updated successfully` })
      }
    })
  } catch (err) {
    res.status(400).send({ status: false, message: "cannot remove item" })
  }
}

//todo: mark an item done
const markItem = (req, res) => {
  try {
    let store = file

    const { todoId, itemId } = req.body

    if (!todoId || !itemId)
      return res.status(400).send({ status: false, message: "id not found" })

    const target = store.todos.filter(todo => todo.id === todoId)

    if (!target.length)
      return res.status(400).send({ status: false, message: "id not exist" })

    //* mark item in todo
    target[0].items.map(
      item => item.id == itemId && (item.isDone = !item.isDone)
    )

    fs.writeFile(fileName, JSON.stringify(store), err => {
      if (err) {
        return res
          .status(400)
          .send({ status: false, message: `writing data error: \n ${err}` })
      } else {
        return res
          .status(200)
          .send({ status: true, message: `updated successfully` })
      }
    })
  } catch (err) {
    res.status(400).send({ status: false, message: "cannot mark item" })
  }
}

//todo: sort item by text
const sortItemByName = (req, res) => {
  try {
    let store = file

    const { todoId, items } = req.body

    if (!todoId)
      return res.status(400).send({ status: false, message: "id not found" })
    
    if (!items)
      return res.status(400).send({ status: false, message: "items not found" })

    //* set sorted items in todo
    store.todos.map(todo => {
      if (todo.id === todoId) {
        todo.items = items
      }
    })

    fs.writeFile(fileName, JSON.stringify(store), err => {
      if (err) {
        return res
          .status(400)
          .send({ status: false, message: `writing data error: \n ${err}` })
      } else {
        return res
          .status(200)
          .send({ status: true, message: `updated successfully` })
      }
    })
  } catch (err) {
    res.status(400).send({ status: false, message: "cannot add item" })
  }
}

//todo: sort items by due date

module.exports = {
  getTodos,
  createTodo,
  removeTodo,
  addItem,
  removeItem,
  markItem,
  sortItemByName,
}
