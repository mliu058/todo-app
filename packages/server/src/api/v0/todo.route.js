const router = require("express").Router()

const {
  getTodos,
  createTodo,
  removeTodo,
  addItem,
  removeItem,
  markItem,
  sortItemByName,
} = require("../../controllers/todo.controller")

router.get("/", getTodos)
router.post("/", createTodo)
router.post("/remove", removeTodo)
router.post("/add-item", addItem)
router.post("/remove-item", removeItem)
router.post("/mark-item", markItem)
router.post("/sort-item-name", sortItemByName)

module.exports = router
