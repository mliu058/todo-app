require("dotenv").config()
const app = require("express")()
const bodyParser = require("body-parser")
const cors = require("cors")
const rateLimit = require("express-rate-limit")

//* routes
const apiRoutes = require("./api/v0")

//* middlewares
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100, // limit each IP to 100 requests per windowMs
})
app.use(limiter)

app.use("/api/v0", apiRoutes)

app.get("/", (req, res) => {
  res.status(200).send("api server is running")
})

app.listen((PORT = 5050), () => {
  console.log(`listening to http://localhost:${PORT}`)
})
